[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

# fMRI preprocessing

fMRI data were preprocessed with FSL 5 (RRID:SCR_002823) 99,100. Pre-processing included motion correction using McFLIRT, smoothing (7mm) and high-pass filtering (.01 Hz) using an 8th order zero-phase Butterworth filter applied using MATLAB’s filtfilt function. We registered individual functional runs to the individual, ANTs brain-extracted T2w images (6 DOF), to T1w images (6 DOF) and finally to 3mm standard space (ICBM 2009c MNI152 nonlinear symmetric) 101 using nonlinear transformations in ANTs 2.1.0 102 (for one participant, no T2w image was acquired and 6 DOF transformation of BOLD data was performed directly to the T1w structural scan). We then masked the functional data with the ICBM 2009c GM tissue prior (thresholded at a probability of 0.25), and detrended the functional images (up to a cubic trend) using SPM12’s spm_detrend. 

We also used a series of extended preprocessing steps to further reduce potential non-neural artifacts 103,104. Specifically, we examined data within-subject, within-run via spatial independent component analysis (ICA) as implemented in FSL-MELODIC 105. Due to the high multiband data dimensionality in the absence of low-pass filtering, we constrained the solution to 30 components per participant. Noise components were identified according to several  key  criteria:  a) Spiking  (components  dominated  by  abrupt  time  series  spikes);  b) Motion (prominent  edge or “ringing” effects, sometimes [but not always] accompanied by large time series spikes); c) Susceptibility and flow artifacts (prominent air-tissue boundary or sinus  activation;  typically  represents  cardio/respiratory  effects); d) White matter (WM) and ventricle  activation 106; e) Low-frequency signal  drift 107; f) High power in high-frequency ranges unlikely to represent neural activity (≥ 75% of total spectral power present above .10 Hz;); and g) Spatial distribution (“spotty” or “speckled” spatial pattern that appears scattered randomly across ≥ 25% of the brain, with few if any clusters with ≥ 80 contiguous voxels). Examples of these various components we typically deem to be noise can be found in 108. By default, we utilized a conservative set of rejection criteria; if manual classification decisions were challenging due to mixing of “signal” and “noise” in a single component, we generally elected to keep such components. Three independent raters of noise components were utilized; > 90% inter-rater reliability was required on separate data before denoising decisions were made on the current data. Components identified as artifacts were then regressed from corresponding fMRI runs using the regfilt command in FSL.

## Functions

**00_prepare_setup:**\
  These scripts create the structure expected by the preprocessing scripts and fill them with the functional and anatomical T1 files. Note that this step was created without originating BIDS data. Using BIDS data from the start would render some of these steps redundant.

**00-07**\
  These scripts represent the main preprocessing pipeline. See `doc/LNDG_PreprocPipeline_README.txt` for more information.

  Note: There is a bug in "preproc2_template.fsf" : set fmri(regstandard_dof) is set to 6 DOF, but should be 12. For the current data, this does not ultimately matter, as we applied nonlinear coregistration to standard space (see scripts in 02B_nonlinregs).

**02B_brainextraction_ANTs**\
  The `preproc2` pipeline implemented an additional coregistration to T2w images. These scripts add T2w images to the preproc data, and apply brain extraction using ANTs.

**02C_nonlinregs**\
  These scripts adapted from Alistair Perry set up the 6DOF coregistration from T1w to T2w, and the nonlinear coregistration to standard images. They then replace the FEAT registrations in the original pipeline with the registrations created in `h_`.

  Note: This step was run after the ICA had been performed using the original coregistration matrices. This may lead to some small deviations, e.g., regarding the background images.

**08_cleanup**\
  This script removed raw input data from the structure to save disk space. `a_` and `f_` scripts should repopulate if necessary.
