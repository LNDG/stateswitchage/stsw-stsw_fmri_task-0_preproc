source preproc2_config.sh

# PBS Log Info
CurrentPreproc="mo_out"
CurrentLog="${LogPath}/${CurrentPreproc}"
if [ ! -d ${CurrentLog} ]; then mkdir -p ${CurrentLog}; chmod 770 ${CurrentLog}; fi
	
FSLDIR=/home/mpib/LNDG/FSL/fsl-5.0.11

# Error log
Error_Log="${CurrentLog}/${CurrentPreproc}_error_summary.txt"; echo "" >> ${Error_Log}; chmod 770 ${CurrentLog}

# Loop over participants, sessions (if they exist) & runs/conditions/tasks/etc
for SUB in ${SubjectID} ; do
	if [ -z "${SessionID}" ]; then Session="NoSessions"; SessionFolder=""; SessionName=""
	else Session="${SessionID}"
	fi
	for SES in ${Session}; do
		if [ "${Session}" != "NoSessions" ]; then
			if [ ! -d ${DataPath}/${SUB}/${SES} ]; then continue
			else SessionFolder="${SES}/"; SessionName="${SES}_"
			fi			
		fi
		for RUN in ${RunID}; do
			
			# Name of anatomical and functional images to be used.
			FuncImage="${SUB}_${SessionName}${RUN}"												# Run specific functional image
			# Path to the anatomical and functional image folders.
			FuncPath="${DataPath}/${SUB}/${SessionFolder}${PreprocPipe}/${RUN}"	# Path for run specific functional image
			
			# Path to the original functional image folder.
			OriginalPath="${DataPath}/${SUB}/mri/${RUN}"
							
			if [ ! -f ${OriginalPath}/${FuncImage}.nii.gz ]; then
				continue
			elif [ -f ${FuncPath}/motionout/${FuncImage}_motionout.txt ]; then
				continue
			fi
					
			# Create output path for motion outlier detection
			
			if [ -d ${FuncPath}/motionout ]; then 
				if [ ! -f ${FuncPath}/motionout/${SUB}_${RUN}_motionout.txt ]; then
					rm -rf ${FuncPath}/motionout
				fi
			fi	
			if [ ! -d ${FuncPath}/motionout ]; then mkdir -p ${FuncPath}/motionout; fi
			
			
			cd ${FuncPath}
			# Gridwise
			echo "#PBS -N ${CurrentPreproc}_${FuncImage}" 						>> job # Job name 
			echo "#PBS -l walltime=12:00:00" 									>> job # Time until job is killed 
			echo "#PBS -l mem=15gb" 											>> job # Books 4gb RAM for the job 
			echo "#PBS -m n" 													>> job # Email notification on abort/end, use 'n' for no notification 
			echo "#PBS -o ${CurrentLog}" 										>> job # Write (output) log to group log folder 
			echo "#PBS -e ${CurrentLog}" 										>> job # Write (error) log to group log folder 

			#echo ". /etc/fsl/5.0/fsl.sh"										>> job # Set fsl environment 	
			
			echo "FSLDIR=/home/mpib/LNDG/FSL/fsl-5.0.11"  >> job
			echo ". ${FSLDIR}/etc/fslconf/fsl.sh"                   >> job
			echo "PATH=${FSLDIR}/bin:${PATH}"                       >> job
			echo "export FSLDIR PATH"                               >> job
			
			echo "cd ${FuncPath}/motionout"           												>> job

			# Run motion outlier detection
			echo "fsl_motion_outliers -i ${OriginalPath}/${FuncImage}.nii.gz -o ${FuncPath}/motionout/${FuncImage}_motionout.txt -s ${FuncPath}/motionout/${FuncImage}_${MoutMetric}.txt -p ${FuncPath}/motionout/${FuncImage}_${MoutMetric}_plot.png --${MoutMetric} --dummy=${DeleteVolumes} -v >> ${FuncPath}/motionout/report.txt" >> job
			
			#echo "fsl_motion_outliers -i ${OriginalPath}/${FuncImage} -o ${FuncPath}/motionout/${SUB}_motionout.txt -s ${FuncPath}/motionout/${SUB}_${MoutMetric}.txt -p ${FuncPath}/motionout/${SUB}_${MoutMetric}_plot.png --${MoutMetric} --dummy=${DeleteVolumes} -v >> ${FuncPath}/motionout/report.txt" >> job
			
			# Error log
			echo "if [ ! -f ${FuncPath}/motionout/${FuncImage}_motionout.txt ];"  		>> job
			echo "then echo 'Error in ${FuncImage}' >> ${Error_Log}; fi"				>> job
			
			echo "chmod -R 770 ${FuncPath}/motionout" >> job
			
			qsub job
			rm job
			
			sleep 10
			
		done
	done
done