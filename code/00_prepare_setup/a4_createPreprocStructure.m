function a4_createPreprocStructure

% Copy Niis into the preprocessign folder. Create proper folder structure.

% 180109 | written by JQK

if ismac
    pn.root              = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/';
else
    pn.root              = '/home/mpib/LNDG/StateSwitch';
end

pn.root_task         = [pn.root, 'WIP/preproc/'];
pn.root_anat         = [pn.root, 'WIP_anat/preproc/'];
pn.preprocData       = [pn.root, 'WIP/preproc/data/preproc/'];
    
%% IDs

% N = 44 (180109); OA: N = 57 (180206);
IDList = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';...
    '1281'; ...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2142';'2145';'2147';'2149';...
    '2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';...
    '2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';...
    '2238';'2241';'2244';'2246';'2248';'2250';'2251';'2252';'2253';'2254';'2255';...
    '2258';'2261'};

fileTypes = {'RUN1'; 'RUN2'; 'RUN3'; 'RUN4'};
fileNames = {'run-1'; 'run-2'; 'run-3'; 'run-4'};

for indID = 1:numel(IDList)
    mkdir([pn.preprocData, IDList{indID}]);
    mkdir([pn.preprocData, IDList{indID}, '/mri/']);
    for indType = 1:4
        disp(['Creating preproc structure: ',fileTypes{indType}, ' Subject ' IDList{indID}]);
        % designate paths
        pn.target = [pn.preprocData, IDList{indID}, '/mri/',fileNames{indType}, '/'];
        mkdir(pn.target);
        fileName = ['sub-STSWD', IDList{indID}, '_' fileNames{indType}];
        fileToCopy = [pn.root_task, 'data/nii/', fileName, '_bold.nii.gz'];
        fileTarget = [pn.target, IDList{indID}, '_', fileNames{indType}, '.nii.gz'];
        if exist(fileToCopy)
            copyfile(fileToCopy, fileTarget);
        end
    end
    % copy anatomical image
    pn.target = [pn.preprocData, IDList{indID}, '/mri/t1/'];
    mkdir(pn.target);
    fileName = ['sub-STSWD', IDList{indID}, '_T1w'];
    fileToCopy = [pn.root_anat, 'data/nii/', fileName, '.nii.gz'];
    fileTarget = [pn.target, IDList{indID}, '_t1.nii.gz'];
    if exist(fileToCopy)
        copyfile(fileToCopy, fileTarget);
    end
end

end