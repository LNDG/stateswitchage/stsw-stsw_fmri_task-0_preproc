#!/bin/bash

# This script prepares tardis by compiling the necessary function in MATLAB.

#ssh tardis # access tardis

# check and choose matlab version
#module avail matlab
module load matlab/R2016b

# compile functions

matlab
%% go to analysis directory containing .m-file
cd('/home/mpib/LNDG/StateSwitch/WIP/preproc/A_scripts/A1_dcm2nii/')
%% compile function and append dependencies
mcc -m A1_dcm2nii.m -a /home/mpib/LNDG/StateSwitch/WIP/preproc/D_tools/dicm2nii/
exit