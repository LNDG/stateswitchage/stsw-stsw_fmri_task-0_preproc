% Copy the DICOM files to the respective directories in the preproc folders

% 180102 | written by JQK

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/';
pn.imageFolders = [pn.root, 'dynamic/raw/C_study/mri/images/'];

IDList = dir(pn.imageFolders);
IDList = {IDList(:).name};
ScanID = cellfun(@(x) x(21:24), IDList(4:end-1), 'un', 0);
IDList = cellfun(@(x) x(6:9), IDList(4:end-1), 'un', 0);
[IDList, c1, c2] = unique(IDList);
IDList = IDList';
ScanID = ScanID(c1)';

IDList = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';'1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';'1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';'2142';'2253';'2254';'2255'};
ScanID = {'7855';'7853';'7854';'7871';'7886';'8041';'7846';'7847';'7859';'7857';'7887';'7888';'7906';'7915';'7913';'7905';'7920';'7996';'8021';'7899';'7904';'7960';'7849';'7889';'7840';'7842';'7848';'7856';'7877';'7861';'7885';'7869';'7875';'7873';'7898';'7903';'7921';'7900';'7907';'7962';'7908';'7916';'7922';'7983';'7860';'7862';'7870';'7872'};

fileTypes = {'RUN1'; 'RUN2'; 'RUN3'; 'RUN4'; 'REST'; 'T1'; 'T2'; 'DTI_6'; 'DTI_64'};
fileFolders = {'task'; 'task'; 'task'; 'task'; 'rest'; 'anat'; 'anat'; 'dwi'; 'dwi'};
fileNames = {'run-1_bold'; 'run-2_bold'; 'run-3_bold'; 'run-4_bold'; 'rest_bold'; 'T1w'; 'T2w'; 'dwi-6dirs'; 'dwi-64dirs'};

% Note: 
% For subject 1126, only the first half was acquired, T2, REST and DTI are missing.
% For subjects 1268 and 1270, T1 and task data are from repeated session
% due to scanner crash during the first MR session.
secondSessionTypes = {'RUN1'; 'RUN2'; 'RUN3'; 'RUN4';'T1'};
firstSessionTypes = {'T2'; 'DTI_6'; 'DTI_64'; 'REST'};

for indType = 8:numel(fileTypes)
    for indID = 1:numel(IDList)
        
        disp(['Copying DICOMs: ',fileTypes{indType}, ' Subject ' IDList{indID}]);
        
        curID = IDList{indID};
        curScanID = ScanID{indID};
        
        % For subject 1126, only the first half was acquired, T2, REST and DTI are missing.
        if max(strcmp(fileTypes{indType}, firstSessionTypes))==1 & strcmp(IDList{indID}, '1126')
            continue
        end
        
        % For subjects 1268 and 1270, use data from the second acquisition.
        if max(strcmp(fileTypes{indType}, secondSessionTypes))==1 & strcmp(IDList{indID}, '1268')
            curScanID = '8020';
        elseif max(strcmp(fileTypes{indType}, secondSessionTypes))==1 & strcmp(IDList{indID}, '1270')
            curScanID = '8039';
        end
        
        % designate paths
        pn.originPN = [pn.imageFolders, 'STSW_',curID,'_STSW_',curID,'_',curScanID, '/'];
        fileName = ['sub-STSWD', IDList{indID}, '_' fileNames{indType}];
        pn.targetPN = [pn.root, 'dynamic/data/mri/', fileFolders{indType}, '/preproc/data/dicom/', fileName];
        
        mkdir([pn.root, 'dynamic/data/mri/', fileFolders{indType}, '/preproc/data/dicom/']);
        
        %mkdir(pn.targetPN); % NOTE: CHECK FOLDER PATHS BEFORE RUNNING.
        
        tmp = dir([pn.originPN, '*/*', fileTypes{indType}, '*']);
        pn.origin = [tmp(1).folder, '/', tmp(1).name]; clear tmp;
        pn.target = [pn.targetPN];
        copyfile(pn.origin, pn.target);
        %zip(pn.target, pn.origin);
                
    end % ID
end % file type
