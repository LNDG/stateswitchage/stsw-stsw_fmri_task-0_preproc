function preproc_detrend_filter_excecute(FuncPath, FuncImage, PolyOrder, TR, HighpassFilterLowCutoff, LowpassFilterHighCutoff, FilterOrder)

disp (['Will attempt to process ' FuncImage ])

cd ([ FuncPath ]);
if ~exist([ FuncImage, '_detrended.nii.gz' ], 'file')
    preproc_detrend( [ FuncPath, '/FEAT.feat/filtered_func_data' ], [FuncPath '/' FuncImage] , PolyOrder, [FuncPath '/FEAT.feat/mask.nii.gz'] ); 
end

cd ([ FuncPath ]);
if ~exist([ FuncImage, '_detrended_bandpassed.nii.gz' ], 'file') || ~exist([ FuncImage, '_detrended_lowpassed.nii.gz' ], 'file')... 
        || ~exist([ FuncImage, '_detrended_highpassed.nii.gz' ], 'file') && exist([ FuncImage, '_detrended.nii.gz' ], 'file')
    preproc_filter ( [ FuncImage, '_detrended' ], TR, num2str(HighpassFilterLowCutoff), num2str(LowpassFilterHighCutoff), FilterOrder, [], [FuncPath '/FEAT.feat/mask.nii.gz']);
end

end
