#!/bin/bash

## Configuration file
# This file will set environments and variables which will be used throughout the preprocessing procedures.

# 170109 | JQK changed path to standards directory, DataPath

## Study parameters

# Project name. This should be the name of the folder in which the study data is saved.
	# EX: ProjectName="Mock_Study"
ProjectName="STSWD"									# No default, must be set by user

# Name of this specific pipeline.
	# EX: "new_preproc"
PreprocPipe="preproc2" 							# Default is preproc

# Set subject ID list. Use an explicit list. No commas between subjects.
	# EX:  SubjectID="Subject001 SubjectID002 Suject003"
#SubjectID="1117 1118 1120 1124 1126 1131 1132 1135 1136 1138 1144 1151 1158 1160 1163 1164 1167 1169 1172 1173 1178 1182 1213 1215 1216 1219 1221 1223 1227 1228 1233 1234 1237 1239 1240 1243 1245 1247 1250 1252 1257 1261 1265 1266 1268 1270 1276 1281"
SubjectID="2104 2107 2108 2112 2118 2120 2121 2123 2125 2129 2130 2131 2132 2133 2134 2135 2139 2140 2142 2145 2147 2149 2157 2160 2201 2202 2203 2205 2206 2209 2210 2211 2213 2214 2215 2216 2217 2219 2222 2224 2226 2227 2236 2237 2238 2241 2244 2246 2248 2250 2251 2252 2253 2254 2255 2258 2261" 

# Set session ID list. Leave as an empty string if no sessions in data path. No commas if containing any session information.
	# EX: SessionID="ses1 ses2"
SessionID="" 									# Default is empty string

# Name of experimental conditions, runs or task data to be analyzed. No commas between runs/conditions.
	# EX: RunID="task1_run1 task1_run2 task2 restingstate"
RunID="run-1 run-2 run-3 run-4" 				# No default, must be set by user

# Voxel size for registration
VoxelSize="3" 									# No default, must be set by user
# TR
TR=".645" 											# No default, must be set by user

## Preprocessing Parameters

# FEAT variables
# TODO: if there are different amounts of TotalVolumes that need to be specified for each image, then this should be manually specified by the user in the 02_preproc_FEAT.sh script. The same thing applies to DeleteVolumes if different amount of deleted volumes have to be specified.

# Primary FEAT variables (neccessary for our analysis)
ToggleMCFLIRT="1"								# 0=No, 1=Yes: Default is 1
BETFunc="1" 									# 0=No, 1=Yes: Default is 1
TotalVolumes="1066" 							# No default, must be set by user
DeleteVolumes="12" 								# Default is 0
HighpassFEAT="0"								# 0=No, 1=Yes: Default is 0
HighpassThreshold="100"							# Arbitrary number, FEAT highpass is not used
SmoothingKernel="7.0" 							# Default is 7.0 (mm)
RegisterStructDOF="6" 							# Default is BBR, other DOF options: 12, 9, 7, 6, 3

# Secondary FEAT variables (normally unused)
NonLinearReg="0"								# 0=No, 1=Yes: Default is 0
NonLinearWarp="10"								# Default is 10, applied only if NonLinearReg=1
IntensityNormalization="0" 						# 0=No, 1=Yes: Default is 0
SliceTimingCorrection="0"						# Default is 0; 0:None,1:Regular up,2:Regular down,3:Use slice order file,4:Use slice timings file,5:Interleaved
Unwarping="0" 									# 0=No, 1=Yes: Default is 0. We do not generally perform B0 unwarping, but allow it here as a funcitonality
EpiSpacing="0.7" 								# Effective EPI echo spacing (ms)
EpiTE="35"										# EPI TE (ms)
UnwarpDir="-x"									# Unwarp direction: x, -x, y, -y, z, -z
SignalLossThresh="10"							# Percentage of signal loss threshold (integer)

# Motion outlier detection metric
MoutMetric="dvars"

# Detrend variables
PolyOrder="3" 									# Default is 3

# Filter variables
HighpassFilterLowCutoff="0.01"					# Default is 0.01, can be set to "off"" if not perforing Highpass
LowpassFilterHighCutoff="off" 					# Default is 0.1, can be set to "off" if not perforing Lowpass
FilterOrder="8" 								# Default is 8

# ICA variables
dimestVALUE="mdl" 								# Default is mdl
bgthresholdVALUE="3" 							# Default is 3
mmthreshVALUE="0.5" 							# Default is 0.5
dimensionalityVALUE="30" 						# Default is 0
AdditionalParameters="-v --Ostats" 				# Default are '-v --Ostats', verbose and, output thresholded maps and probability maps

# FIX variables
# TestSetID="1126 1151 1167 1216 1239 1270 2104 2121 2250 2202 2215 2236 1120 1131 1243 1247 1245 2130 2139 2134 2248 2214 2206 1172 1173 1266 1250 1261 2129 2145 2108 2201 2210 2203 1135 1169 1257 1281 2123 2147 2213 2258"

FixThreshold=""									# Accepted FIX Threshold, must be determined by the user

# Additional parameters
StandardsAndMasks="standards" 					# Used for FEAT & FLIRT Registration

## Set directories

BaseDirectory="$(pwd)/../.."
BaseDirectory=$(builtin cd $BaseDirectory; pwd)

## Project directories
ProjectDirectory="${BaseDirectory}"  							# Base project directory
ScriptsPath="${ProjectDirectory}/code/b_${PreprocPipe}" 	# Pipe specific scripts
LogPath="${ProjectDirectory}/log/${PreprocPipe}"			# Common log paths for pipe
ToolboxPath="${ProjectDirectory}/tools" 						# Common Toolboxes
StandardPath="${ProjectDirectory}/data/standards" 			# Standards
DataPath="${ProjectDirectory}/data/preproc"  			# Data directory

# Initiate project logs
if [ ! -d ${LogPath} ]; then mkdir -p ${LogPath}; chmod 770 ${LogPath}; fi