#!/bin/bash

rootpath="$(pwd)/../.."
rootpath=$(builtin cd $rootpath; pwd)

cd ${rootpath}/data/preproc/

subs=($(find . -maxdepth 1 -type d | cut -c 3-))

for subject in "${subs[@]}" ; do
for run in {1..4} ; do
	echo ${subject}
	echo ${run}
	filename="${subject}_run-${run}_feat_detrended_highpassed_denoised_MNI3mm.nii.gz"
	rm ${rootpath}/data/preproc/${subject}/preproc2/run-${run}/${filename}
	filename="${subject}_run-${run}_feat_detrended_highpassed_denoised_t2nlreg_2_3mm.nii"
	rm ${rootpath}/data/preproc/${subject}/preproc2/run-${run}/${filename}
	filename="${subject}_run-${run}_feat_detrended_highpassed_denoised_t2nlreg_2009c_3mm.nii"
	gzip ${rootpath}/data/preproc/${subject}/preproc2/run-${run}/${filename}
done
done