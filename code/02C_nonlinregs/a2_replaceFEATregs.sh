#!/bin/bash

source ./setup.sh

subj=$1 # e.g., "1117"
WORKDIR=$2 # e.g., "$(pwd)/../../data/preproc"

for j in $(seq 1 1 4); do

    #backup original reg folder as red-orig if backup doesn't exist already
    if [ ! -d "${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg-orig/" ]; 
    then
        echo "Saving original reg from being overwritten"
        mkdir -p ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg-orig/
        rsync -vaz ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/ ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg-orig/highres2standard.mat
    else 
        echo "Backup exists, replacing files in reg-orig ..."
    fi

    echo "copying standard"

    cp ${WORKDIR}/../standards/2009c_brain_str.nii ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/standard.nii
    gzip -f ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/standard.nii
    #fslswapdim ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/standard.nii.gz RL PA IS 
    #fslreorient2std ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/standard.nii.gz

    echo "copying highres"

    cp -f ${WORKDIR}/${subj}/preproc2/nlregs/${subj}_t1_ANTs_brain_str.nii.gz ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/highres.nii.gz
    #fslswapdim ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/highres.nii.gz RL PA IS 
    #fslreorient2std ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/highres.nii.gz

    #Convert each funct run transform

    echo "creating example_func2highres"

    c3d_affine_tool \
        -ref ${WORKDIR}/${subj}/preproc2/nlregs/run-${j}/${subj}_meanfunc_in_t2_to_t1_Warped.nii.gz \
        -src ${WORKDIR}/${subj}/preproc2/nlregs/run-${j}/example_func_masked.nii.gz \
        -itk ${WORKDIR}/${subj}/preproc2/nlregs/run-${j}/${subj}_meanfunc_in_t2_to_t10GenericAffine.mat \
        -ras2fsl \
        -o ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/example_func2highres.mat

    echo "creating highres2standard"

    c3d_affine_tool \
        -ref ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/standard.nii.gz \
        -src ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/highres.nii.gz \
        -itk ${WORKDIR}/${subj}/preproc2/nlregs/${subj}_t1_ANTs_in_2009c0GenericAffine.mat \
        -ras2fsl \
        -o ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/highres2standard.mat

    echo "creating standard2highres"

    convert_xfm \
        -omat ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/standard2highres.mat \
        -inverse ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/highres2standard.mat

    #generate example of functional to high-res using ants and fsl

    echo "creating example_func2highres"

    antsApplyTransforms \
        -d 3 \
        -r ${WORKDIR}/${subj}/preproc2/nlregs/${subj}_t1_ANTs_brain_str.nii.gz \
        -i ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/example_func.nii.gz \
        -t ${WORKDIR}/${subj}/preproc2/nlregs/run-${j}/${subj}_meanfunc_in_t2_to_t10GenericAffine.mat \
        -o ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/example_func2highres.nii.gz

    # flirt \
    #     -ref ${WORKDIR}/${subj}/preproc2/nlregs/${subj}_t1_ANTs_brain_str.nii.gz \
    #     -in ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/example_func.nii.gz \
    #     -applyxfm \
    #     -init ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/example_func2highres.mat \
    #     -out ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/example_func2highres_verify

    echo "creating highres2standard_verify"

    # antsApplyTransforms \
    #     -d 3 \
    #     -r ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/standard.nii.gz \
    #     -i ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/highres.nii.gz \
    #     -t ${WORKDIR}/${subj}/preproc2/nlregs/${subj}_t1_ANTs_in_2009c0GenericAffine.mat \
    #     -o ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/highres2standard_verify_ants.nii.gz

    # flirt \
    #     -ref ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/standard.nii.gz \
    #     -in ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/highres.nii.gz \
    #     -applyxfm \
    #     -init ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/highres2standard.mat \
    #     -out ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/highres2standard_verify_fsl.nii.gz
    
    #verify standard to highres 

    echo "creating standard2highres_verify"

    flirt \
        -ref ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/highres.nii.gz \
        -in ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/standard.nii.gz \
        -applyxfm \
        -init ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/standard2highres.mat \
        -out ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat/reg/standard2highres_verify

    #update FEAT reg overview

    updatefeatreg ${WORKDIR}/${subj}/preproc2/run-${j}/FEAT.feat -pngs 

done
