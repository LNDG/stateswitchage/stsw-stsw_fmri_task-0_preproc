#!/bin/bash

export OMP_NUM_THREADS=8

#Load all required software
module load mrtrix3/3.0.0-rc3
module load freesurfer/6.0.0
module load ants/2.1.0-mpib0
#module load fsl/5.0.11 # For some reason compiled fsl throws errors

FSLDIR=/home/mpib/LNDG/FSL/fsl-5.0.11
. ${FSLDIR}/etc/fslconf/fsl.sh
PATH=${FSLDIR}/bin:${PATH}
PATH_C3D=/home/mpib/LNDG/toolboxes/c3dtool/bin
export FSLDIR PATH=${PATH_C3D}:$PATH