#!/bin/bash

fun_name="a1_ants_regs.sh"
job_name="a1_ants_regs"

rootpath="$(pwd)/../.."
rootpath=$(builtin cd $rootpath; pwd)

# path to the text file with all subject ids:
path_ids="${rootpath}/code/IDs.txt"
# read subject ids from the list of the text file
IDS=$(cat ${path_ids} | tr '\n' ' ')

mkdir ${rootpath}/log/${job_name}

TEMPimage="${rootpath}/data/standards/2009c_brain_str.nii"
TEMPimage_lowres="${rootpath}/data/2009c_brain_3mm_str.nii"
TEMPpre="2009c"

for subj in ${IDS}; do
	sbatch \
  		--job-name ${job_name}_${subj} \
  		--cpus-per-task 8 \
  		--mem 6gb \
  		--time 00:15:00 \
  		--output ${rootpath}/log/${job_name}/${job_name}_${subj}.out \
  		--workdir . \
  		--wrap=". /etc/profile ; $(pwd)/${fun_name} ${subj} ${rootpath}/data/preproc ${TEMPimage} ${TEMPimage_lowres} ${TEMPpre} "
done 