#!/bin/bash

fun_name="a2_replaceFEATregs.sh"
job_name="a2_replaceFEATregs"

rootpath="$(pwd)/../.."
rootpath=$(builtin cd $rootpath; pwd)

# path to the text file with all subject ids:
path_ids="${rootpath}/code/IDs.txt"
# read subject ids from the list of the text file
IDS=$(cat ${path_ids} | tr '\n' ' ')

mkdir ${rootpath}/log/${job_name}

for subj in ${IDS}; do
#for subj in "1117"; do
	sbatch \
  		--job-name ${job_name}_${subj} \
  		--cpus-per-task 1 \
  		--mem 2gb \
  		--time 00:15:00 \
  		--output ${rootpath}/log/${job_name}/${job_name}_${subj}.out \
  		--workdir . \
  		--wrap=". /etc/profile ; $(pwd)/${fun_name} ${subj} ${rootpath}/data/preproc "
done
