#!/bin/bash

## Brain Extraction (BET) using ANTs. Create brain extracted images using desired parameters.

# 180516 | adapted from José's FSL BET script by JQK

source ./../preproc_config.sh

# PBS Log Info
CurrentPreproc="BET_ANTs"
CurrentLog="${LogPath}/${CurrentPreproc}"

# ANTs-specific paths and files
PathANTSScript="${ScriptsPath}/01_preproc_BET_ANTs"
PathTemplImg="${StandardPath}/ANTS/MICCAI2012-Multi-Atlas-Challenge-Data/T_template0.nii.gz"
PathProbImg="${StandardPath}/ANTS/MICCAI2012-Multi-Atlas-Challenge-Data/T_template0_BrainCerebellumProbabilityMask.nii.gz"
PathRegMask="/${StandardPath}/ANTS/MICCAI2012-Multi-Atlas-Challenge-Data/T_template0_BrainCerebellumRegistrationMask.nii.gz"

if [ ! -d ${CurrentLog} ]; then mkdir ${CurrentLog}; chmod 770 ${CurrentLog}; fi

# Loop over participants & sessions (if they exist)
for SUB in ${SubjectID} ; do

	#sleep $(( RANDOM % 60 )) # sleep between 1 and 60 seconds to avoid heavy I/O loads

	if [ -z "${SessionID}" ]; then Session="NoSessions"; SessionFolder=""; SessionName=""
	else Session="${SessionID}"
	fi
	for SES in ${Session}; do

		if [ "${Session}" != "NoSessions" ]; then
			if [ ! -d ${DataPath}/${SUB}/${SES} ]; then continue
			else SessionFolder="${SES}/"; SessionName="${SES}_"
			fi			
		fi	
		
		# Path to the anatomical image folder.
		AnatPath="${DataPath}/${SUB}/${SessionFolder}mri/t1"		# Path for anatomical image
		# Name of anatomical image to be used.
		AnatImage="${SUB}_${SessionName}t1" 						# Original anatomical image, no extraction performed
		cd ${AnatPath}
		if [ ! -f ${AnatImage}.nii.gz ]; then
			AnatImage=`ls co*`														
		fi
		
		# Verifies if the anatomical image exists. If it doesn't, the for loop stops here and continues with the next item.
		if [ ! -f ${AnatPath}/${AnatImage}.nii.gz ]; then
			echo "No mprage: ${SUB} cannot be processed"
			continue
		fi
		
		# Gridwise
		echo "#PBS -N ${CurrentPreproc}_${SessionName}${SUB}" 	>> job # Job name 
		echo "#PBS -l walltime=06:00:00" 						>> job # Time until job is killed 
		echo "#PBS -l mem=10gb" 								>> job # Books 10gb RAM for the job 
		echo "#PBS -m n" 										>> job # Email notification on abort/end, use 'n' for no notification 
		echo "#PBS -o ${CurrentLog}" 							>> job # Write (output) log to group log folder 
		echo "#PBS -e ${CurrentLog}" 							>> job # Write (error) log to group log folder 
    	
    	if [ -d ${AnatPath}/tempANTs ]; then rm -rf ${AnatPath}/tempANTs; fi # remove prior temp folder if it exists

    	
    	echo "module load ants/2.2.0" >> job
		
		echo "mkdir ${AnatPath}/tempANTs"						>> job

		# Perform Brain Extraction
		
		echo "${PathANTSScript}/antsBrainExtraction.sh -d 3 -a ${AnatPath}/${AnatImage}.nii.gz -e ${PathTemplImg} -m ${PathProbImg} -f ${PathRegMask} -k 1 -o ${AnatPath}/tempANTs/${AnatImage}_ANTsBET_" >> job

		echo "chmod -R 770 ."  		>> job

		qsub job
		rm job
	done
done

