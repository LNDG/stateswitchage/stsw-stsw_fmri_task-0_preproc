% Copy ANTs BETs from temporary folders into the structural main folder

% N = 44 (180102);OA: N = 57 (180206; including 4 pilots);
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';...
    '1215';'1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';...
    '1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';...
    '1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2142';'2145';...
    '2147';'2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';...
    '2210';'2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';...
    '2226';'2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';...
    '2251';'2252';'2253';'2254';'2255';'2258';'2261'};

pn.root = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/preproc/';

for indID = 1:numel(IDs)
    try
    curID = IDs{indID}; disp(['Processing subject ', curID])

    pn.anatomical = [pn.root, '/B_data/D_preproc/',curID,'/mri/t2/'];
    ANTsBETfileName = [curID, '_T2w_ANTsBET_BrainExtractionBrain.nii.gz'];

    source = [pn.anatomical, 'tempANTs/',ANTsBETfileName];
    target = [pn.anatomical, curID, '_T2w_ANTs_brain.nii.gz'];

    copyfile(source, target)
    
%     if exist([pn.anatomical, ANTsBETfileName])==2
%         delete([pn.anatomical,ANTsBETfileName])
%     end
%     if exist([pn.anatomical, curID, '_T2w_ANTs_brain'])==2
%         delete([pn.anatomical, curID, '_T2w_ANTs_brain'])
%     end
    catch
    end
end