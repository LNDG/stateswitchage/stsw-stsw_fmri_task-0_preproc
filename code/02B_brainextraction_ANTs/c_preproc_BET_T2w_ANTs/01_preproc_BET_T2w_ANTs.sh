#!/bin/bash

## Brain Extraction (BET) using ANTs. Create brain extracted images using desired parameters.

PreprocPipe="F_createT2wCoreg"	
#SubjectID="1117 1118 1120 1124 1125 1126 1131 1132 1135 1136 1138 1144 1151 1158 1160 1163 1164 1167 1169 1172 1173 1178 1182 1213 1214 1215 1216 1219 1221 1223 1227 1228 1233 1234 1237 1239 1240 1243 1245 1247 1250 1252 1257 1261 1265 1266 1268 1270 1276 1281 2104 2107 2108 2112 2118 2120 2121 2123 2125 2129 2130 2131 2132 2133 2134 2135 2139 2140 2142 2145 2147 2149 2157 2160 2201 2202 2203 2205 2206 2209 2210 2211 2213 2214 2215 2216 2217 2219 2222 2224 2226 2227 2236 2237 2238 2241 2244 2246 2248 2250 2251 2252 2253 2254 2255 2258 2261" 									# No default, must be set by user
SubjectID="2130 2226 2206"
SessionID=""

# Not available data: 1126 (T2w missing), 1138, 1144, 1158, 1163, 1213, 1221 (for the rest, no folder is available)

## Set directories

ProjectDirectory="/home/mpib/LNDG/StateSwitch/WIP/preproc"  # Base project directory
ScriptsPath="${ProjectDirectory}/A_scripts/${PreprocPipe}" 	# Pipe specific scripts
StandardPath="${ProjectDirectory}/B_data/C_standards"  		# Template directory
LogPath="${ProjectDirectory}/Y_logs/${PreprocPipe}"			# Common log paths for pipe
DataPath="${ProjectDirectory}/B_data/D_preproc"  			# Data directory

# Initiate project logs
if [ ! -d ${LogPath} ]; then mkdir -p ${LogPath}; chmod 770 ${LogPath}; fi

#############################################
####### START ANTs-specific script ##########
#############################################

# PBS Log Info
CurrentPreproc="BET_ANTs"
CurrentLog="${LogPath}/${CurrentPreproc}"

# ANTs-specific paths and files
PathANTSScript="${ScriptsPath}/01_preproc_BET_T2w_ANTs"
PathTemplImg="${StandardPath}/ANTS/MICCAI2012-Multi-Atlas-Challenge-Data/T_template0.nii.gz"
PathProbImg="${StandardPath}/ANTS/MICCAI2012-Multi-Atlas-Challenge-Data/T_template0_BrainCerebellumProbabilityMask.nii.gz"
PathRegMask="${StandardPath}/ANTS/MICCAI2012-Multi-Atlas-Challenge-Data/T_template0_BrainCerebellumRegistrationMask.nii.gz"

if [ ! -d ${CurrentLog} ]; then mkdir ${CurrentLog}; chmod 770 ${CurrentLog}; fi

# Loop over participants & sessions (if they exist)
for SUB in ${SubjectID} ; do

	#sleep $(( RANDOM % 60 )) # sleep between 1 and 60 seconds to avoid heavy I/O loads

	if [ -z "${SessionID}" ]; then Session="NoSessions"; SessionFolder=""; SessionName=""
	else Session="${SessionID}"
	fi
	for SES in ${Session}; do

		if [ "${Session}" != "NoSessions" ]; then
			if [ ! -d ${DataPath}/${SUB}/${SES} ]; then continue
			else SessionFolder="${SES}/"; SessionName="${SES}_"
			fi			
		fi	
		
		# Path to the anatomical image folder.
		AnatPath="${DataPath}/${SUB}/${SessionFolder}mri/t2"		# Path for anatomical image
		# Name of anatomical image to be used.
		AnatImage="${SUB}_${SessionName}T2w" 						# Original anatomical image, no extraction performed
		cd ${AnatPath}
		if [ ! -f ${AnatImage}.nii.gz ]; then
			AnatImage=`ls co*`														
		fi
		
		# Verifies if the anatomical image exists. If it doesn't, the for loop stops here and continues with the next item.
		if [ ! -f ${AnatPath}/${AnatImage}.nii.gz ]; then
			echo "No mprage: ${SUB} cannot be processed"
			continue
		fi
		
		# Gridwise
		echo "#PBS -N ${CurrentPreproc}_${SessionName}${SUB}" 	>> job # Job name 
		echo "#PBS -l walltime=06:00:00" 						>> job # Time until job is killed 
		echo "#PBS -l mem=10gb" 								>> job # Books 10gb RAM for the job 
		echo "#PBS -m n" 										>> job # Email notification on abort/end, use 'n' for no notification 
		echo "#PBS -o ${CurrentLog}" 							>> job # Write (output) log to group log folder 
		echo "#PBS -e ${CurrentLog}" 							>> job # Write (error) log to group log folder 
    	
    	if [ -d ${AnatPath}/tempANTs ]; then rm -rf ${AnatPath}/tempANTs; fi # remove prior temp folder if it exists

    	
    	echo "module load ants/2.2.0" >> job
		
		echo "mkdir ${AnatPath}/tempANTs"						>> job

		# Perform Brain Extraction
		
		echo "${PathANTSScript}/antsBrainExtraction.sh -d 3 -a ${AnatPath}/${AnatImage}.nii.gz -e ${PathTemplImg} -m ${PathProbImg} -f ${PathRegMask} -k 1 -o ${AnatPath}/tempANTs/${AnatImage}_ANTsBET_ -c 3x3x2x1" >> job

		echo "chmod -R 770 ."  		>> job

		qsub job
		rm job
	done
done

