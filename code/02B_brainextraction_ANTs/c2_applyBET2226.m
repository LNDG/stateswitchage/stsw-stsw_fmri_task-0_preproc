% Create 2226 image from T2w and BET mask

% Note: Input image appears to have strange header.

pn.tools	= '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B_PLS/T_tools/';  addpath(genpath(pn.tools));

pn.root = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/preproc/B_data/D_preproc/2226/mri/t2/';

T2w = load_untouch_nii([pn.root, '2226_T2w.nii.gz']);

T2wBET = load_untouch_nii([pn.root, 'tempANTs/2226_T2w_ANTsBET_BrainExtractionPriorWarped.nii.gz']);

T2w.img = double(T2w.img).*double(T2wBET.img);

save_untouch_nii(T2w,[pn.root, '2226_T2w_ANTs_brain.nii.gz'])

