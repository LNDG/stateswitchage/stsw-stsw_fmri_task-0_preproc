% compare DVARS and IC outlier output
clear all
cd('/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/A_scripts/B3_preproc2/motion_outlier_processing')

ID = {'1117' '1118' '1120' '1124' '1125' '1126' '1131' '1132' '1135' '1136' '1151' '1160' '1164' '1167' '1169' '1172' '1173' '1178' '1182' '1214' '1215' '1216' '1219' '1223' '1227' '1228' '1233' '1234' '1237' '1239' '1240' '1243' '1245' '1247' '1250' '1252' '1257' '1261' '1265' '1266' '1268' '1270' '1276' '1281' '2104' '2107' '2108' '2112' '2118' '2120' '2121' '2123' '2125' '2129' '2130' '2131' '2132' '2133' '2134' '2135' '2139' '2140' '2142' '2145' '2147' '2149' '2157' '2160' '2201' '2202' '2203' '2205' '2206' '2209' '2210' '2211' '2213' '2214' '2215' '2216' '2217' '2219' '2222' '2224' '2226' '2227' '2236' '2237' '2238' '2241' '2244' '2246' '2248' '2250' '2251' '2252' '2253' '2254' '2255' '2258' '2261'};
run_ID = {'run-1' 'run-2' 'run-3' 'run-4'};

thresh = 3; % IC SD outlier threshold

%load IC outlier report
load(['/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/A_scripts/B3_preproc2/motion_outlier_processing/IC_outlier_thresh-' num2str(thresh) '.mat'],'out');

for sub = 1:length(ID)
    
    %initialise for concatenation across runs
    IC_out_ind=[];
    
    % load DVARS output
    DVARS_ind=(load(['/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/' ID{sub} '/preproc/' ID{sub} '_motionout_scol_all.txt']))';
    
    for run = 1:length(run_ID)
        
        %load IC outlier output
        try
            IC_out_ind_run=out.(['sub' ID{sub}]).(['run' run_ID{run}(5)]).multi_outlier_ind;
            IC_out_ind=[IC_out_ind IC_out_ind_run];   
        catch
            fprintf('No DVARS file found for sub %s %s\n',ID{sub},run_ID{run})
            continue
        end
        
    end
    
        % summaries for each method
        DVARS_vol=find(DVARS_ind);
        DVARS_N = sum(DVARS_ind);
        
        IC_out_vol=find(IC_out_ind);
        IC_out_N=sum(IC_out_ind);
    
        %find overlap and divergence
        DVARS_IC_overlap_vol = intersect(DVARS_vol,IC_out_vol);
        DVARS_IC_overlap_N = length(DVARS_IC_overlap_vol);
        DVARS_not_IC_vol = setdiff(DVARS_vol,IC_out_vol);
        DVARS_not_IC_N = length(DVARS_not_IC_vol);
        IC_not_DVARS_vol = setdiff(IC_out_vol,DVARS_vol);
        IC_not_DVARS_N = length(IC_not_DVARS_vol);
        
        % add info to output structure  
        comp.(['sub' ID{sub}]).DVARS_IC_overlap_vol = DVARS_IC_overlap_vol;
        comp.(['sub' ID{sub}]).DVARS_IC_overlap_N = DVARS_IC_overlap_N;
        comp.(['sub' ID{sub}]).DVARS_not_IC_vol = DVARS_not_IC_vol;
        comp.(['sub' ID{sub}]).DVARS_not_IC_N = DVARS_not_IC_N;
        comp.(['sub' ID{sub}]).IC_not_DVARS_vol = IC_not_DVARS_vol;
        comp.(['sub' ID{sub}]).IC_not_DVARS_N = IC_not_DVARS_N;
        comp.(['sub' ID{sub}]).DVARS_ind = DVARS_ind;
        comp.(['sub' ID{sub}]).DVARS_vol = DVARS_vol;
        comp.(['sub' ID{sub}]).DVARS_N = DVARS_N;
        comp.(['sub' ID{sub}]).IC_out_ind = IC_out_ind;
        comp.(['sub' ID{sub}]).IC_out_vol = IC_out_vol;
        comp.(['sub' ID{sub}]).IC_out_N = IC_out_N;
end

% save stuff
save(['/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/A_scripts/B3_preproc2/motion_outlier_processing/comp_DVARS_IC_thr-' num2str(thresh) '_out.mat'],'comp')

% save as table
tab_mat=[];

for sub = 1:length(ID)
   
    tab_mat(sub,:)=[comp.(['sub' ID{sub}]).DVARS_IC_overlap_N,comp.(['sub' ID{sub}]).DVARS_not_IC_N,comp.(['sub' ID{sub}]).IC_not_DVARS_N,comp.(['sub' ID{sub}]).DVARS_N,comp.(['sub' ID{sub}]).IC_out_N];
    
end

tab=array2table(tab_mat,'VariableNames',{'DVARS_IC_overlap_N' 'DVARS_not_IC_N' 'IC_not_DVARS_N' 'DVARS_N' 'IC_out_N'}, 'RowNames', ID);
writetable(tab,['/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/A_scripts/B3_preproc2/motion_outlier_processing/comp_DVARS_IC_thr-' num2str(thresh) '_tab.xls'])
