% mean time course dvars example

% undenoised 2217 run-4
t_undenoised=niftiread('/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/2217/preproc2/run-4/2217_run-4_feat_detrended_highpassed.nii.gz');

denom=size(t_undenoised,1)+size(t_undenoised,2)+size(t_undenoised,3);

avg_t_undenoised=squeeze(sum(t_undenoised,1));
avg_t_undenoised=squeeze(sum(avg_t_undenoised,1));
avg_t_undenoised=squeeze(sum(avg_t_undenoised,1));

avg_t_undenoised=avg_t_undenoised./denom;

dvars_bin=load('/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/2217/preproc2/run-4/motionout_post/2217_run-4_feat_detrended_highpassed_denoised_motionout_post_scol.txt');
dvars_bin_old=load('/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/2217/preproc/run-4/motionout/2217_motionout_scol.txt');

plot(avg_t_undenoised)
hold on
line(repmat((find(dvars_bin))',2,1),repmat(ylim',1,length(find(dvars_bin))),'Color',[1,0,1,0.5])
line(repmat((find(dvars_bin_old))',2,1),repmat(ylim',1,length(find(dvars_bin_old))),'Color',[1,0,0,0.5])
title('sub-2217 run-4 undenoised')

% denoised 2217 run-4
t_denoised=niftiread('/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/2217/preproc2/run-4/2217_run-4_feat_detrended_highpassed_denoised.nii.gz');

denom=size(t_denoised,1)+size(t_denoised,2)+size(t_denoised,3);

avg_t_denoised=squeeze(sum(t_denoised,1));
avg_t_denoised=squeeze(sum(avg_t_denoised,1));
avg_t_denoised=squeeze(sum(avg_t_denoised,1));

avg_t_denoised=avg_t_denoised./denom;

dvars_bin=load('/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/2217/preproc2/run-4/motionout_post/2217_run-4_feat_detrended_highpassed_denoised_motionout_post_scol.txt');
dvars_bin_old=load('/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/2217/preproc/run-4/motionout/2217_motionout_scol.txt');

plot(avg_t_denoised)
hold on
line(repmat((find(dvars_bin))',2,1),repmat(ylim',1,length(find(dvars_bin))),'Color',[1,0,1,0.5])
line(repmat((find(dvars_bin_old))',2,1),repmat(ylim',1,length(find(dvars_bin_old))),'Color',[1,0,0,0.5])
title('sub-2217 run-4 denoised')

%%
% undenoised 1270 run-3
clear all
t_undenoised=niftiread('/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/1270/preproc2/run-3/1270_run-3_feat_detrended_highpassed.nii.gz');

denom=size(t_undenoised,1)+size(t_undenoised,2)+size(t_undenoised,3);

avg_t_undenoised=squeeze(sum(t_undenoised,1));
avg_t_undenoised=squeeze(sum(avg_t_undenoised,1));
avg_t_undenoised=squeeze(sum(avg_t_undenoised,1));

avg_t_undenoised=avg_t_undenoised./denom;

dvars_bin=load('/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/1270/preproc2/run-3/motionout_post/1270_run-3_feat_detrended_highpassed_denoised_motionout_post_scol.txt');
dvars_bin_old=load('/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/1270/preproc/run-3/motionout/1270_motionout_scol.txt');

plot(avg_t_undenoised)
hold on
line(repmat((find(dvars_bin))',2,1),repmat(ylim',1,length(find(dvars_bin))),'Color',[1,0,1,0.5])
line(repmat((find(dvars_bin_old))',2,1),repmat(ylim',1,length(find(dvars_bin_old))),'Color',[1,0,0,0.5])

% denoised 1270 run-3
t_denoised=niftiread('/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/1270/preproc2/run-3/1270_run-3_feat_detrended_highpassed_denoised.nii.gz');

denom=size(t_denoised,1)+size(t_denoised,2)+size(t_denoised,3);

avg_t_denoised=squeeze(sum(t_denoised,1));
avg_t_denoised=squeeze(sum(avg_t_denoised,1));
avg_t_denoised=squeeze(sum(avg_t_denoised,1));

avg_t_denoised=avg_t_denoised./denom;

dvars_bin=load('/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/1270/preproc2/run-3/motionout_post/1270_run-3_feat_detrended_highpassed_denoised_motionout_post_scol.txt');
dvars_bin_old=load('/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/1270/preproc/run-3/motionout/1270_motionout_scol.txt');

plot(avg_t_denoised)
hold on
line(repmat((find(dvars_bin))',2,1),repmat(ylim',1,length(find(dvars_bin))),'Color',[1,0,1,0.5])
line(repmat((find(dvars_bin_old))',2,1),repmat(ylim',1,length(find(dvars_bin_old))),'Color',[1,0,0,0.5])

%%
% undenoised 2142 run-3
clear all
t_undenoised=niftiread('/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/2142/preproc2/run-3/2142_run-3_feat_detrended_highpassed.nii.gz');

denom=size(t_undenoised,1)+size(t_undenoised,2)+size(t_undenoised,3);

avg_t_undenoised=squeeze(sum(t_undenoised,1));
avg_t_undenoised=squeeze(sum(avg_t_undenoised,1));
avg_t_undenoised=squeeze(sum(avg_t_undenoised,1));

avg_t_undenoised=avg_t_undenoised./denom;

dvars_bin=load('/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/2142/preproc2/run-3/motionout_post/2142_run-3_feat_detrended_highpassed_denoised_motionout_post_scol.txt');
dvars_bin_old=load('/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/2142/preproc/run-3/motionout/2142_motionout_scol.txt');

plot(avg_t_undenoised)
hold on
line(repmat((find(dvars_bin))',2,1),repmat(ylim',1,length(find(dvars_bin))),'Color',[1,0,1,0.5])
line(repmat((find(dvars_bin_old))',2,1),repmat(ylim',1,length(find(dvars_bin_old))),'Color',[1,0,0,0.5])
title('sub-2142 run-3 undenoised')

% denoised 2142 run-3
t_denoised=niftiread('/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/2142/preproc2/run-3/2142_run-3_feat_detrended_highpassed_denoised.nii.gz');

denom=size(t_denoised,1)+size(t_denoised,2)+size(t_denoised,3);

avg_t_denoised=squeeze(sum(t_denoised,1));
avg_t_denoised=squeeze(sum(avg_t_denoised,1));
avg_t_denoised=squeeze(sum(avg_t_denoised,1));

avg_t_denoised=avg_t_denoised./denom;

dvars_bin=load('/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/2142/preproc2/run-3/motionout_post/2142_run-3_feat_detrended_highpassed_denoised_motionout_post_scol.txt');
dvars_bin_old=load('/Volumes/FB-LIP/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/2142/preproc/run-3/motionout/2142_motionout_scol.txt');

plot(avg_t_denoised)
hold on
line(repmat((find(dvars_bin))',2,1),repmat(ylim',1,length(find(dvars_bin))),'Color',[1,0,1,0.5])
line(repmat((find(dvars_bin_old))',2,1),repmat(ylim',1,length(find(dvars_bin_old))),'Color',[1,0,0,0.5])
title('sub-2142 run-3 denoised')
