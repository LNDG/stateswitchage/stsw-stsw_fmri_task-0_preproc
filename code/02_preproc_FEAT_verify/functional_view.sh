#!/bin/bash

# 180111 | JQK added FuncImage definition

source ../preproc2_config.sh

if [ ! -d ${ScriptsPath}/extras/functional ]; then mkdir -p ${ScriptsPath}/extras/functional ; fi

Files=""

# Loop over participants, sessions (if they exist) & runs/conditions/tasks/etc
for SUB in ${SubjectID} ; do
	if [ -z "${SessionID}" ]; then Session="NoSessions" ; SessionFolder="" ; SessionName=""
	else Session="${SessionID}"
	fi	
	for SES in ${Session}; do
		if [ "${Session}" != "NoSessions" ]; then
			if [ ! -d ${DataPath}/${SUB}/${SES} ]; then continue
			else SessionFolder="${SES}/"; SessionName="${SES}_"
			fi			
		fi
		for RUN in ${RunID}; do
			
			# Path to the original functional image folder.
			OriginalPath="${DataPath}/${SUB}/${SessionFolder}mri/${RUN}"
			# Path to the pipeline specific folder.
			FuncPath="${DataPath}/${SUB}/${SessionFolder}${PreprocPipe}/${RUN}"	
			# Name of functional image to be used.
			FuncImage="${SUB}_${SessionName}${RUN}"	
			
			if [ ! -f ${OriginalPath}/${FuncImage}.nii.gz ]; then
				continue
			elif [ ! -f ${FuncPath}/FEAT.feat/filtered_func_data.nii.gz ]; then
				continue
			fi
	
			Files="${Files} ${FuncPath}/FEAT.feat/filtered_func_data.nii.gz"
		done
	done
done

# Setup FSL
source /etc/fsl/5.0/fsl.sh

# Create functional image viewer with MNI overlay
cd ${ScriptsPath}/extras/functional
slicesdir ${Files}
